<?php namespace App\Console\Commands;

use App\Events\SendTrackEmail;
use App\Track;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendEmails extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'SendEmails';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$tracks = Track::where('serverTime', date('H'))->get();
        $tracks->filter(function($element){

            if($element->disabled){
                return false;
            }

            if($element->schedule == 'daily'){
                return true;
            }


            if($element->schedule == 'weekly'){
                if(strtotime($element->last_called) > strtotime('-7 days')){
                    return true;
                }
            }

            if($element->schedule == 'monthly'){
                if(strtotime($element->last_called) > strtotime('-30 days')){
                    return true;
                }
            }

            return false;
        });
        foreach($tracks as $track){
            $track->last_called = date('Y-m-d H:i:s');
            $track->save();
            event(new SendTrackEmail($track));
        }

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		];
	}

}
