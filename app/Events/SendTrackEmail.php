<?php namespace App\Events;

use App\Events\Event;

use App\Track;
use Illuminate\Queue\SerializesModels;

class SendTrackEmail extends Event {

	use SerializesModels;

    public $track;
    public $user;
    /**
     * Create a new event instance.
     *
     * @param Track $track
     */
	public function __construct(Track $track)
	{
		$this->track = $track;
        $this->user = $track->user;
	}

}
