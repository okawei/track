<?php namespace App\Handlers\Events;

use App\Events\SendTrackEmail;

use App\MailgunClient;
use App\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Support\Facades\Mail;

class TrackEmailWasSent {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  SendTrackEmail  $event
	 * @return void
	 */
	public function handle(SendTrackEmail $event)
	{
        $firstName = explode(' ', $event->user->name)[0];
        $html = view('emails.track', ['track'=>$event->track, 'name'=>$firstName])->render();
        $mailgunClient = new MailgunClient();
        $result = $mailgunClient->send($html, $event->user->email, 'Your '.$event->track->name.' track is ready.');
        Message::create([
            'trackId'=>$event->track->id,
            'messageId'=>$result->http_response_body->id
        ]);

	}

}
