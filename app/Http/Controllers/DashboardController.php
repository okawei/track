<?php namespace App\Http\Controllers;

use App\Point;
use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tracks = Track::where('userId', $request->user()->id)->orderBy('order','ASC')->paginate(15);

        return view('dashboard.dashboard', ['tracks'=>$tracks]);
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function getNew()
    {
        $images = scandir(app_path().'/../public/images/tracks');
        $images = array_slice($images, 2);

        return view('dashboard.newTrack', ['images'=>$images]);
    }


    public function viewTrack($id){
        $track = Track::find($id);
        if($this->canAccessTrack($track) !== true){
            return $this->canAccessTrack($track);
        }
        $scale = 15;

        $points = $track->getPointsArray($scale);
        $total = array_sum($points);
        $average = 0;
        if(count($points) != 0){
            $average = $total/count($points);
        }



        $median = 0;
        if(count($points) != 0){
            $median = $points;
            arsort($median);
            $median = $median[floor(count($points)/2)];
        }

        $paginatedPoints = Point::where('trackId', $track->id)->orderBy('created_at', 'ASC')->paginate($scale);

        return view('dashboard.viewTrack', ['track'=>$track, 'scale'=>$scale, 'average'=>$average, 'median'=>$median, 'points'=>$paginatedPoints]);
    }

    public function getAccount(){
        return view('dashboard.account');
    }

    public function getEdit($id){
        $track = Track::find($id);
        if($this->canAccessTrack($track) !== true){
            return $this->canAccessTrack($track);
        }
        $timezone = $track->timezone;
        $correctTime = new \DateTime($track->time, new \DateTimeZone(date_default_timezone_get()));

        $correctTime = $correctTime->setTimezone(new \DateTimeZone($track->timezone));

        $images = scandir(app_path().'/../public/images/tracks');
        $images = array_slice($images, 2);
        return view('dashboard.editTrack', ['images'=>$images, 'track'=>$track, 'correctTime'=>$correctTime]);
    }

    /**
     * @param $track
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function canAccessTrack($track)
    {
        if ($track == null) {
            return redirect('/home')->with('error', 'You do not have permission to access that track!');
        }
        if ($track->userId != Auth::user()->id) {
            return redirect('/home')->with('error', 'You do not have permission to access that track!');
        }
        return true;
    }
}
