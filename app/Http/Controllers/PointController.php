<?php namespace App\Http\Controllers;



use App\Message;
use App\Point;
use App\Track;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class PointController extends Controller {

    public function setPoint($secureId, $value){
        $track = Track::where('secureId', $secureId)->first();
        if($this->validateSecureid($track) !== true){
            return $this->validateSecureid($track);
        }

        $lastPoint = Point::where('trackId', $track->id)->orderBy('created_at', 'desc')->first();
        if($lastPoint != null){
            $createdAt = $lastPoint->created_at->setTimezone($track->timezone)->format('Y-m-d');
            $createdAt = Carbon::createFromFormat('Y-m-d', $createdAt);
            if(abs($createdAt->day - Carbon::today()->day) == 0){
                Session::set('message', [false, 'Point Already Set!', 'You\'ve already added a data point for today!  The point you added was '.$lastPoint->points.'. If you would like to change this, please <a href="'.URL::to('/track/point/update/'.$lastPoint->id).'">click here</a>.']);
                Session::set('pointSecureId', $track->secureId);
                return redirect('/message');
            }
        }

        $point = Point::create([
            'trackId'=>$track->id,
            'points'=>$value
        ]);

        Session::set('message', [true, 'Successfully added data point!', 'Your data point has been successfully added.  Thanks for using track!']);
        return redirect('/message');
    }

    public function getUpdate($pointId){
        $secureId = Session::get('pointSecureId');
        $track = Track::where('secureId', $secureId)->first();
        $point = Point::find($pointId);
        if($this->validatePointUpdate($track, $point) !== true){
            return $this->validatePointUpdate($track, $point);
        }



        return view('points.update', ['track'=>$track, 'point'=>$point]);



    }

    public function postUpdate($pointId, $value){
        $secureId = Session::get('pointSecureId');
        $track = Track::where('secureId', $secureId)->first();
        $point = Point::find($pointId);
        if($this->validatePointUpdate($track, $point) !== true){
            return $this->validatePointUpdate($track, $point);
        }

        $point->points = $value;
        $point->save();
        Session::forget('pointSecureId');
        Session::set('message', [true, 'Successfully updated data point!', 'Your data point has been successfully updated.  Thanks for using track!']);
        return redirect('/message');
    }

    public function getCustom($secureId){
        $track = Track::where('secureId', $secureId)->first();
        if($this->validateSecureid($track) !== true){
            return $this->validateSecureid($track);
        }
        $lastPoint = Point::where('trackId', $track->id)->orderBy('created_at', 'desc')->first();
        if($lastPoint != null){

            $createdAt = $lastPoint->created_at->setTimezone($track->timezone)->format('Y-m-d');
            $createdAt = Carbon::createFromFormat('Y-m-d', $createdAt);
            if(abs($createdAt->day - Carbon::today()->day) == 0){
                Session::set('message', [false, 'Point Already Set!', 'You\'ve already added a data point for today!  The point you added was '.$lastPoint->points.'. If you would like to change this, please <a href="'.URL::to('/track/point/update/'.$lastPoint->id).'">click here</a>.']);
                Session::set('pointSecureId', $track->secureId);
                return redirect('/message');
            }
        }

        return view('points.custom', ['track'=>$track]);
    }

    public function postCustom(Request $request, $secureId){
        $track = Track::where('secureId', $secureId)->first();
        if($this->validateSecureid($track) !== true){
            return $this->validateSecureid($track);
        }

        $point = Point::create([
            'trackId'=>$track->id,
            'points'=>$request->input('value')
        ]);

        Session::set('message', [true, 'Successfully added data point!', 'Your data point has been successfully added.  Thanks for using track!']);
        return redirect('/message');

    }

    public function getCustomUpdate($pointId){
        $secureId = Session::get('pointSecureId');
        $track = Track::where('secureId', $secureId)->first();
        $point = Point::find($pointId);
        if($this->validatePointUpdate($track, $point) !== true){
            return $this->validatePointUpdate($track, $point);
        }
        return view('points.custom', ['track'=>$track, 'point'=>$point]);
    }

    public function postCustomUpdate(Request $request, $pointId){
        $secureId = Session::get('pointSecureId');
        $track = Track::where('secureId', $secureId)->first();
        $point = Point::find($pointId);
        if($this->validatePointUpdate($track, $point) !== true){
            return $this->validatePointUpdate($track, $point);
        }
        $point->points = $request->input('value');
        $point->save();

        Session::forget('pointSecureId');
        Session::set('message', [true, 'Successfully updated data point!', 'Your data point has been successfully updated.  Thanks for using track!']);
        return redirect('/message');
    }

    public function filterResponse(Request $request){
        $messageId = $request->input('In-Reply-To');
        if($messageId == null){
            return;
        }
        $message = Message::where('messageId', $messageId);
        $track = $message->track;
        $lastPoint = Point::where('trackId', $track->id)->orderBy('created_at', 'desc')->first();
        if($lastPoint != null){

            $createdAt = $lastPoint->created_at->setTimezone($track->timezone)->format('Y-m-d');
            $createdAt = Carbon::createFromFormat('Y-m-d', $createdAt);
            if(abs($createdAt->day - Carbon::today()->day) == 0){
                return;
            }
        }
        $value = $request->input('stripped-text');
        if($value == null){
            return;
        }
        preg_match_all('!\d+!', $value, $valueNumerical);
        $valueNumerical = (float)$valueNumerical;
        $point = Point::create([
            'trackId'=>$track->id,
            'points'=>$valueNumerical
        ]);

    }

    /**
     * @param $track
     * @param $point
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function validatePointUpdate($track, $point)
    {
        if ($track == null || $point == null) {
            Session::set('message', [false, 'Track Not Found!', 'Sorry, the track you were looking for was not found.  Please check the URL and try again later.']);
            return redirect('/message');
        }

        if ($track->id != $point->trackId) {
            Session::set('message', [false, 'Track Not Found!', 'Sorry, the track you were looking for was not found.  Please check the URL and try again later.']);
            return redirect('/message');
        }

        return true;
    }

    /**
     * @param $track
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function validateSecureid($track)
    {
        if ($track == null) {
            Session::set('message', [false, 'Track Not Found!', 'Sorry, the track you were looking for was not found.  Please check the URL and try again later.']);
            return redirect('/message');
        }
        return true;
    }


}
