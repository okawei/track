<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Track;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;


class TrackController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */




    public function postNew(Request $request){

        $this->middleware('auth');
        $timezone = $request->input('timezone');
        $correctTime = new \DateTime($request->input('time'), new \DateTimeZone($timezone));


        $track = Track::create($request->input());
        $track->userId = Auth::user()->id;
        $track->time = $correctTime->format('ga');
        $correctTime = $correctTime->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        $track->serverTime = $correctTime->format('H');
        $track->secureId = str_random(64);
        $track->save();

        return redirect('/home')->with('success','Successfully created track '.$track->name);
    }

    public function postEdit(Request $request, $id){

        $this->middleware('auth');
        $track = Track::find($id);
        if($this->canAccessTrack($track) !== true){
            return $this->canAccessTrack($track);
        }

        $timezone = $request->input('timezone');
        $correctTime = new \DateTime($request->input('time'), new \DateTimeZone($timezone));

        $track->update($request->input());
        $track->time = $correctTime->format('ga');
        $correctTime = $correctTime->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        $track->serverTime = $correctTime->format('H');
        $track->save();

        return redirect('/home')->with('success','Successfully updated track '.$track->name);
    }

    public function pause(Request $request, $id){
        $this->middleware('auth');
        $track = Track::where('id', $id)->where('userId',$request->user()->id)->first();
        if($track == null){
            return redirect()->back()->with('error', 'You do not have permission to edit that track!');
        }
        $track->disabled = true;
        $track->save();
        return redirect()->back()->with('success', 'Successfully paused track!');
    }

    public function start(Request $request, $id){
        $this->middleware('auth');
        $track = Track::where('id', $id)->where('userId',$request->user()->id)->first();
        if($track == null){
            return redirect()->back()->with('error', 'You do not have permission to edit that track!');
        }
        $track->disabled = false;
        $track->save();
        return redirect()->back()->with('success', 'Successfully started track!');
    }

    public function updateOrders(Request $request){
        $this->middleware('auth');
        $orders = $request->input('orders');
        $index = 0;
        foreach($orders as $order){
            $track = Track::find($order);
            if($track->userId != Auth::user()->id){
                continue;
            }

            $track->order = $index;
            $track->save();

            $index++;

        }
    }

    public function deleteTrack($id){
        $this->middleware('auth');
        $track = Track::find($id);
        if($this->canAccessTrack($track) !== true){
            return $this->canAccessTrack($track);
        }

        $track->delete();
        return redirect('/home')->with('success', 'Successfully removed track');
    }

    public function getUnsubscribe($secureId){
        $track = Track::where('secureId', $secureId)->first();
        if($track == null){
            Session::set('message', [false, 'Track Not Found!', 'Sorry, the track you were looking for was not found.  Please check the URL and try again later.']);
            return redirect('/message');
        }

        Session::set('message', [true, 'Are you sure?', 'Are you sure you would like to unsubscribe from the track '.$track->name.'? <br><br> <a class="btn btn-danger" href="'.URL::to('/track/unsubscribe/'.$track->secureId.'/doit').'">Yes, unsubscribe</a>']);
        return redirect('/message');
    }

    public function unsubscribe($secureId){
        $track = Track::where('secureId', $secureId)->first();
        if($track == null){
            Session::set('message', [false, 'Track Not Found!', 'Sorry, the track you were looking for was not found.  Please check the URL and try again later.']);
            return redirect('/message');
        }
        $track->disabled = true;
        $track->save();

        Session::set('message', [true, 'Success!', 'Successfully unsubscribed from track '.$track->name.'!']);
        return redirect('/message');
    }


    /**
     * @param $track
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function canAccessTrack($track)
    {
        $this->middleware('auth');
        if ($track == null) {
            return redirect('/home')->with('error', 'You do not have permission to access that track!');
        }
        if ($track->userId != Auth::user()->id) {
            return redirect('/home')->with('error', 'You do not have permission to access that track!');
        }
        return true;
    }

}
