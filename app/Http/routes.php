<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Delete;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

Route::get('/', 'WelcomeController@index');
Route::get('/message', function(){
    $message = Session::get('message');
    return view('message', ['message'=>$message]);
});

Route::get('home', 'DashboardController@index');

Route::get('/account', 'DashboardController@getAccount');

Route::get('track/new', 'DashboardController@getNew');
Route::post('track/new', 'TrackController@postNew');

Route::get('/signup/social/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('/signup/social/facebook/register', 'Auth\AuthController@handleProviderCallback');

Route::get('/truncate', function(){

    Model::unguard();
    \App\Track::truncate();
    \App\Point::truncate();
    \App\User::truncate();
});

Route::get('/seed', function(){
    $seeder = new DatabaseSeeder();
    $seeder->run();
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/flush', function(){
    Session::flush();
});

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('/');
});

Route::get('/track/pause/{id}', 'TrackController@pause');
Route::get('/track/start/{id}', 'TrackController@start');

Route::post('/track/updateOrders', 'TrackController@updateOrders');

Route::get('/track/{id}', 'DashboardController@viewTrack');

Route::get('/track/edit/{id}', 'DashboardController@getEdit');
Route::post('/track/edit/{id}', 'TrackController@postEdit');
Route::get('/track/delete/{id}', 'TrackController@deleteTrack');

Route::get('/track/unsubscribe/{secureId}', 'TrackController@getUnsubscribe');
Route::get('/track/unsubscribe/{secureId}/doit', 'TrackController@unsubscribe');

Route::get('/track/point/update/{id}', 'PointController@getUpdate');
Route::get('/track/point/update/{id}/custom', 'PointController@getCustomUpdate');
Route::post('/track/point/update/{id}/custom', 'PointController@postCustomUpdate');
Route::get('/track/point/update/{id}/{value}', 'PointController@postUpdate');
Route::get('/track/point/{secureId}/custom', 'PointController@getCustom');
Route::post('/track/point/{secureId}/custom', 'PointController@postCustom');
Route::get('/track/point/{secureId}/{value}', 'PointController@setPoint');

Route::post('/filterResponse', 'PointController@filterResponse');
