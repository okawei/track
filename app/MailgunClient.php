<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Mailgun\Mailgun;

class MailgunClient {

    private $key = 'key-ecb28beedeb8fe0014088840db820dd0';
    private $domain = 'trackemail.me';
    private $mg;

    public function __construct($key = null, $domain = null){
        if($key != null){
            $this->key = $key;
        }
        if($domain != null){
            $this->domain = $domain;
        }

        $this->mg = new Mailgun($this->key);
    }


    public function send($html, $to, $subject){
        $result = $this->mg->sendMessage($this->domain, array(
            'to'=>$to,
            'from'=> Config::get('mail.from.name') . ' <' . Config::get('mail.from.address') . '>',
            'html'=>$html,
            'subject'=>$subject
        ));
        return $result;
    }
}