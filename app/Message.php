<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Message extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['trackId', 'messageId'];


    public function track(){
        return $this->belongsTo('App\Track', 'trackId');
    }

}
