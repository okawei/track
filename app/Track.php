<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class Track extends Model {


    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tracks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'question', 'start', 'finish', 'interval', 'schedule', 'time', 'disabled', 'image', 'units', 'timezone'];


    public static function create(array $data){
        if(!Auth::check()){
            throw new \Exception('You cannot create a track when logged out');
        }
        return parent::create($data);
    }

    public function user(){
        return $this->belongsTo('App\User', 'userId');
    }

    public function points(){
        return $this->hasMany('App\Point', 'trackId');
    }

    public function getPointsArray(){
        $points = $this->points;
        $pointsArray = $points->map(function($point){
            return $point->points;
        });
        return $pointsArray->toArray();

    }

    public function getPointsArrayString($days){
        $pointsArray = $this->getPointsArray();
        $pointsArray = array_slice($pointsArray, 0, $days);
        $pointsArrayString = implode(', ', $pointsArray);
        $pointsArrayString = '['.$pointsArrayString.']';
        return $pointsArrayString;
    }

    public function getLabelsArray(){
        $points = $this->points;
        $pointsLabelsArray = $points->map(function($point){
            return date('m/d', strtotime($point->created_at));
        });
        return $pointsLabelsArray->toArray();
    }

    public function getLabelsArrayString($days){
        $pointsArray = $this->getLabelsArray();
        $pointsArray = array_slice($pointsArray, 0, $days);
        $pointsArrayString = implode('", "', $pointsArray);
        $pointsArrayString = '["'.$pointsArrayString.'"]';
        return $pointsArrayString;
    }

}

