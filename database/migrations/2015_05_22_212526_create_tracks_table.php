<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
            $table->string('secureId');
            $table->integer('userId');
            $table->string('name');
            $table->string('question');
            $table->integer('start');
            $table->integer('finish');
            $table->integer('interval');
            $table->string('schedule');
            $table->string('time');
            $table->string('image');
            $table->string('units');
            $table->boolean('disabled')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracks');
	}

}
