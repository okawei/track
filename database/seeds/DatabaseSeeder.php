<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        \App\Track::truncate();
        \App\Point::truncate();
		 $this->call('TrackSeeder');
	}

}

class TrackSeeder extends Seeder{
    public function run(){
        $faker = \Faker\Factory::create();
        $images = scandir(app_path().'/../public/images/tracks');
        $images = array_slice($images, 2);

        for($x = 0; $x < 4; $x++){
            $track = \App\Track::create([
                'name'=>$faker->word,
                'secureId'=>str_random(64),
                'question'=>$faker->sentence,
                'start'=>$faker->randomElement([0,1]),
                'finish'=>$faker->randomElement([5,10]),
                'interval'=>$faker->randomElement([1,10,100,1000]),
                'schedule'=>'daily',
                'time'=>$faker->randomElement(["12pm", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12am", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"]),
                'disabled'=>false,
                'image'=>$faker->randomElement($images)
            ]);
            $userId = \App\User::first()->id;

            $track->userId = $userId;
            $track->save();
        }

        for($x = 0; $x < 4; $x++){
            $track = \App\Track::create([
                'name'=>$faker->word,
                'secureId'=>str_random(64),
                'question'=>$faker->sentence,
                'start'=>$faker->randomElement([0,1]),
                'finish'=>$faker->randomElement([5,10]),
                'interval'=>$faker->randomElement([1,10,100,1000]),
                'schedule'=>'daily',
                'time'=>$faker->randomElement(["12pm", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12am", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"]),
                'disabled'=>false,
                'image'=>$faker->randomElement($images)
            ]);

            $track->userId = 2;
            $track->save();
        }


        for($x = 0; $x < 4; $x++){
            $track = \App\Track::create([
                'name'=>$faker->word,
                'secureId'=>str_random(64),
                'question'=>$faker->sentence,
                'start'=>$faker->randomElement([0,1]),
                'finish'=>$faker->randomElement([5,10]),
                'interval'=>$faker->randomElement([1,10,100,1000]),
                'schedule'=>'daily',
                'time'=>$faker->randomElement(["12pm", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12am", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"]),
                'disabled'=>false,
                'image'=>$faker->randomElement($images)
            ]);

            $track->userId = 3;
            $track->save();
        }


        for($x = 0; $x < 4; $x++){
            $track = \App\Track::create([
                'name'=>$faker->word,
                'secureId'=>str_random(64),
                'question'=>$faker->sentence,
                'start'=>$faker->randomElement([0,1]),
                'finish'=>$faker->randomElement([5,10]),
                'interval'=>$faker->randomElement([1,10,100,1000]),
                'schedule'=>'daily',
                'time'=>$faker->randomElement(["12pm", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12am", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"]),
                'disabled'=>false,
                'image'=>$faker->randomElement($images)
            ]);

            $track->userId = 4;
            $track->save();
        }

        foreach(\App\Track::all() as $track){
            foreach(range(0, rand(1, 45)+30) as $index){
                $data = rand($track->start, $track->finish)*$track->interval;
                $point = \App\Point::create([
                    'trackId'=>$track->id,
                    'points'=>$data
                ]);
                $point->created_at = \Carbon\Carbon::now()->subDays($index);
                $point->save();
            }
        }
    }
}
