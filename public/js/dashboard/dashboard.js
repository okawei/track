$(function () {

    var index = 0;
    $('.trackChart').each(function(){

        $(this).highcharts({
            title: {
                text: ''
            },
            chart:{

            },
            backgroundColor: null,
            tooltip: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: [],
                lineWidth: 0,
                text: '',
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                labels: {
                    enabled: false
                },
                minorTickLength: 0,
                tickLength: 0
            },
            yAxis: {
                gridLineColor: 'transparent',
                title: {
                    text: ''
                },
                labels:
                {
                    enabled: false
                }
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    },
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                }
            },

            series: [{
                data: dataSets[index] ,
                showInLegend: false,
                lineWidth: 5,
                lineColor: 'rgba(52,152,219,1)'
            }]
        });
        index++;
        $(this).parent().find('.name').addClass('color'+index);
    })
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$('.pauseTrack').click(function(){
    var trackName = $(this).parent().parent().find('.name').text().trim();
    var trackId = $(this).parent().attr('data-id');

    swal({   title: "Are you sure?",   text: "Are you sure you would like to pause the track '"+trackName+"'? You will no longer receive emails for this track but all data will still be stored.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, pause it!",   closeOnConfirm: false }, function(){
        window.location = pauseURL+"/"+trackId
    });
})

$('.startTrack').click(function(){
    var trackName = $(this).parent().parent().find('.name').text().trim();
    var trackId = $(this).parent().attr('data-id');

    swal({   title: "Are you sure?",   text: "Are you sure you would like to start the track '"+trackName+"'?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, start it!",   closeOnConfirm: false }, function(){
        window.location = startURL+"/"+trackId
    });
})

$('.editTrack').click(function(){
    var trackId = $(this).parent().attr('data-id');
    window.location = editURL+"/"+trackId;
})

$('.tracks').sortable({
    stop: function(event, ui){
        var orders = [];
        $('.draggable').each(function(){
            orders.push($(this).data('index'));
        })

        $.ajax({
            method: "POST",
            url: "/track/updateOrders",
            data: { _token: csrf_token, orders: orders }
        })
    }
});