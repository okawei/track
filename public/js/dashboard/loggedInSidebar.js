var sidebarShowing = false;
$('.hamburger').click(function(){
    if(sidebarShowing){
        $('.sidebar').animate({
            marginLeft: "-220px"
        })
        $('.showSidebar').show();
        $('.hideSidebar').hide();
    } else{
        $('.sidebar').animate({
            marginLeft: "0px"
        })
        $('.hideSidebar').show();
        $('.showSidebar').hide();
    }
    sidebarShowing = !sidebarShowing;
})