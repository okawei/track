function range(start, end) {
    var foo = [];
    for (var i = start; i <= end; i++) {
        foo.push(i);
    }
    return foo;
}

var viewModel = function(){
    var self = this;

    self.interval = ko.observable();
    self.start = ko.observable();
    self.finish = ko.observable();
    self.schedule = ko.observable();
    self.time = ko.observable();
    self.imageName = ko.observable();
    self.imagePath = ko.observable();

    self.timezone = ko.observable();

    self.start.subscribe(function(){
        self.finish(undefined);
    })

    self.intervalPreviewItems = ko.computed(function(){
        return range(parseInt(self.start()), parseInt(self.finish())).map(function(x) { return x * parseInt(self.interval()); });;
    });

    self.finishOptions = ko.computed(function(){
        if(self.start() != undefined && self.start() != ''){
            return  range(parseInt(self.start())+1, 10);
        }
        return [];
    })

}

var vm = new viewModel();
var timezone = jstz.determine();
vm.timezone =timezone.name();


ko.applyBindings(vm);


$('.selectImage').click(function(){
    $('.coverUp').show();
    $('.trackImageSelect').show();
    $(window).scrollTop(100);
    $('body, html').css('overflow', 'hidden');
});

$('.image').click(function(){
    var imageName = $(this).attr('data-value');
    var imagePath = $(this).attr('data-path');
    vm.imageName(imageName);
    vm.imagePath(imagePath);
    $('.image').hide();
    $('.trackImageSelect').css('background-image', 'url("'+imagePath+'")');
    $('.imageSelectHeader').hide();
    $('.confirmation').fadeIn();
})

$('.no').click(function(){
    vm.imageName(undefined);
    vm.imagePath(undefined);
    $('.confirmation').hide();
    $('.image').show();
    $('.imageSelectHeader').show();
    $('.trackImageSelect').css('background-image', 'none');
})

$('.yes').click(function(){
    $('.selectedImage').css('background-image', 'url("'+vm.imagePath()+'")');
})

$('.yes, .close, .coverUp').click(function(){
    $('.confirmation').hide();
    $('.image').show();
    $('.imageSelectHeader').show();
    $('.trackImageSelect').css('background-image', 'none');
    $('.trackImageSelect').hide();
    $('.coverUp').hide();
    $('body, html').css('overflow', 'auto');
})

$('.remove').click(function(){
    vm.imageName(undefined);
    vm.imagePath(undefined);
})

$('.change').click(function(){
    vm.imageName(undefined);
    vm.imagePath(undefined);
    $('.coverUp').show();
    $('.trackImageSelect').show();
    $(window).scrollTop(100);
    $('body, html').css('overflow', 'hidden');
})