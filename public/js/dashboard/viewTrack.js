$(document).ready(function(){
    $('.title').animate({
        marginTop: '-300px',
        opacity: '1'
    }, 1000)
})
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$('.pauseTrack').click(function(){
    swal({   title: "Are you sure?",   text: "Are you sure you would like to pause the track '"+trackName+"'? You will no longer receive emails for this track but all data will still be stored.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, pause it!",   closeOnConfirm: false }, function(){
        window.location = pauseURL+"/"+trackId
    });
})


$('.startTrack').click(function(){

    swal({   title: "Are you sure?",   text: "Are you sure you would like to start the track '"+trackName+"'?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, start it!",   closeOnConfirm: false }, function(){
        window.location = startURL+"/"+trackId
    });
})

$('.deleteTrack').click(function(){
    swal({   title: "Are you sure?",   text: "Are you sure you would like to delete the track '"+trackName+"'? This cannot be undone.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){
        window.location = '/track/delete/'+trackId;
    });
})


var mainLineChartCtx = document.getElementById("mainLineChart").getContext("2d");
var barChartCtx = document.getElementById("barChart").getContext("2d");

var lineChartData = {
    labels: labels,
    datasets: [
        {
            label: "My First dataset",
            fillColor: fillColor,
            strokeColor: strokeColor,
            pointColor: "transparent",
            pointStrokeColor: "transparent",
            pointHighlightFill: "rgba(41, 128, 185,1)",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: data
        }
    ]
};


var mainLineChart = new Chart(mainLineChartCtx).Line(lineChartData, {
    scaleShowGridLines : false,
    scaleShowLabels : false,
    tooltipTemplate: toolTipTemplate,
});


var barChartData = {
    labels: new Array(labels.length).join('.').split('.'),
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.15)",
            strokeColor: "rgba(220,220,220,0.15)",
            highlightFill: "rgba(220,220,220,0.15)",
            highlightStroke: "rgba(220,220,220,0.15)",
            data: data
        }
    ]
};

var barChart = new Chart(barChartCtx).Bar(barChartData, {
    scaleShowGridLines : false,
    scaleShowLabels : false,
    tooltipTemplate: "<%= value + ' "+units+"' %>",
    showTooltips: false,
    animation: false
});

