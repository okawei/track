

@extends('layouts.loggedOut')

@section('header')
@section('title') Track - Log In @stop
    <link rel="stylesheet" href="{{asset('css/landing/login.css')}}"/>
@stop

@section('content')
    <div class="colors">
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
    </div>
    <div class="container">


        <div class="row">
            <div class="header col-md-10 col-md-offset-1">
                <h2>Login</h2>

                @if (count($errors) > 0)
                    <div class="alert alert-danger errors">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <div class="col-md-4 col-md-offset-1 socialLogin">
                <Br>
                <a href="{{URL::to('/signup/social/facebook')}}">
                    <img src="{{asset('images/landing/login-facebook.png')}}">
                </a>
            </div>
            <div class="col-md-6">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}" data-parsley-validate data-parsley-trigger="change">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="form-group">
                        <label>E-Mail Address</label>

                        <input type="email" name="email" value="{{ old('email') }}">

                    </div>

                    <div class="form-group">
                        <label>Password</label>

                        <input type="password" name="password">

                    </div>

                    <div class="form-group" class="rememberBox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Login</button>

                        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('bower/parsleyjs/dist/parsley.min.js')}}"></script>

@stop
