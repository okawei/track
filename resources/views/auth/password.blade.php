@extends('layouts.loggedOut')

@section('header')
    @section('title') Track - Password Reset @stop
    <link rel="stylesheet" href="{{asset('css/landing/login.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/landing/password.css')}}"/>
@stop

@section('content')
<div class="container">
	<div class="row">

        <div class="header col-md-12">
            <h2>Forgot Your Password?</h2>
            <h3>Enter your email to reset it.</h3>

            @if (count($errors) > 0)
                <div class="alert alert-danger errors">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
		<div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif


            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>E-Mail Address</label>
                        <input type="email" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Send Password Reset Link
                    </button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection
