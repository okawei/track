

@extends('layouts.loggedOut')

@section('header')
    @section('title') Track - Sign Up @stop
    <link rel="stylesheet" href="{{asset('css/landing/register.css')}}"/>
@stop

@section('content')
    <div class="colors">
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
        <div class="color"></div>
    </div>
<div class="container">


	<div class="row">
        <div class="header col-md-10 col-md-offset-1">
            <h2>Register</h2>
            <h3>Create an account or sign in using social media</h3>

            @if (count($errors) > 0 || Session::has('error'))
                <div class="alert alert-danger errors">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <ul>
                        {{Session::get('error')}}
                    </ul>
                </div>
            @endif

        </div>
        <div class="col-md-4 col-md-offset-1 socialLogin">

            <a href="{{URL::to('/signup/social/facebook')}}">
                <img src="{{asset('images/landing/login-facebook.png')}}">
            </a>
        </div>
		<div class="col-md-6">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}" data-parsley-validate data-parsley-trigger="change">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>Name</label>
                    <input type="text"  name="name" value="{{ old('name') }}" required>
                </div>

                <div class="form-group">
                    <label>E-Mail Address</label>
                    <input type="email"  name="email" value="{{ old('email') }}" required>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password"  name="password" required data-parsley-equalto="#password_confirmation">
                </div>

                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password"  name="password_confirmation" id="password_confirmation" required>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('footer')
    <script src="{{asset('bower/parsleyjs/dist/parsley.min.js')}}"></script>

@stop
