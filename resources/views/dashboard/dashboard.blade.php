@extends('layouts.loggedInSidebar')

@section('title')
    Track - Dashboard
@stop

@section('header')
    <link rel="stylesheet" href="{{asset('bower/sweetalert/dist/sweetalert.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/dashboard/dashboard.css')}}"/>
@stop

@section('content')

    <div class="container-fluid">
        <div class="tracks">
        @foreach($tracks as $track)
            <a class="draggable" href="{{URL::to('/track/'.$track->id)}}" data-index="{{$track->id}}">
                <div class="track @if($track->disabled)paused @endif">
                    <div class="controls" data-id="{{$track->id}}">
                        @if($track->disabled)
                            <i onclick="return false" class="fa fa-play-circle startTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Enable"></i>
                        @else
                            <i onclick="return false" class="fa fa-minus-circle pauseTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Disable"></i>
                        @endif
                        <i onclick="return false" onclick="javascript:window.location='{{URL::to('/track/'.$track->id.'/edit')}}'" class="fa fa-pencil editTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Edit"></i>
                    </div>
                    @if($track->disabled)
                        <span class="disabled">
                            PAUSED
                        </span>

                    @endif
                    <span class="name">
                        {{$track->name}}
                        <span class="schedule">{{$track->schedule}} at {{$track->time}}</span>
                    </span>
                    <span class="question">
                        {{$track->question}}
                    </span>


                    <div class="trackChart"></div>
                    @if($track->image != null)
                    <img src="{{asset('/images/tracks/'.$track->image)}}">
                    @else
                        <div class="defaultBackground"></div>
                    @endif

                    <div class="colors">
                        <div class="color"></div>
                        <div class="color"></div>
                        <div class="color"></div>
                        <div class="color"></div>
                        <div class="color"></div>
                    </div>
                </div>
            </a>
        @endforeach
        @if(count($tracks) == 0)
            <div class="noTracks">
                <h2>You do not currently have any tracks!</h2>
                <h3>To create a track click the "New Track" button to the left.</h3>
            </div>
            @for($x = 0; $x < 3; $x++)
                <div class="ghostTrack">
                    <div class="line"></div>
                    <div class="line"></div>

                    <div class="bottomLine"></div>
                </div>

            @endfor
        @endif
        </div>
    </div>

@stop

@section('footer')

    <script src="{{asset('bower/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('bower/highcharts/highcharts.src.js')}}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
        var dataSets = [
            @foreach($tracks as $track)
            {{$track->getPointsArrayString(15)}},
            @endforeach
        ]
        var pauseURL = "{{URL::to('/track/pause/')}}";
        var startURL = "{{URL::to('/track/start/')}}";
        var editURL = "{{URL::to('/track/edit/')}}";
        var csrf_token = '{{csrf_token()}}';
    </script>
    <script src="{{asset('js/dashboard/dashboard.js')}}"></script>

    @if(Session::has('success'))
        <script>
            swal({   title: "Success!",   text: "{{Session::get('success')}}",   type: "success",   confirmButtonText: "Sweet!" });
        </script>
    @endif
    @if(Session::has('error'))
        <script>
            swal({   title: "Error!",   text: "{{Session::get('error')}}",   type: "error",   confirmButtonText: "Well shoot..." });
        </script>
    @endif

@stop