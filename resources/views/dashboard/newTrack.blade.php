@extends('layouts.loggedInSidebar')

@section('title')
    Track - Dashboard
@stop

@section('header')
    <link rel="stylesheet" href="{{asset('css/dashboard/newTrack.css')}}"/>
@stop

@section('beforeContent')

    <div class="coverUp"></div>
    <div class="trackImageSelect">
        <h1 class="imageSelectHeader">Select An Image</h1>
        <div class="close">X</div>
        @foreach($images as $image)
            <div class="image" style="background-image: url('{{asset('images/tracks/'.$image)}}')" data-value="{{$image}}" data-path="{{asset('images/tracks/'.$image)}}">
                <div class="select hvr-underline-from-center select{{rand(1,5)}}">
                    <i class="fa fa-check"></i>
                </div>
            </div>
        @endforeach

        <div class="confirmation">
            <h2>Use this image?</h2>
            <a class="btn btn-success yes">Yes</a>
            <a class="btn btn-danger no">No</a>
        </div>
    </div>


@stop

@section('content')
    <div class="container-fluid">
        <h1>Create New Track</h1>

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/track/new') }}" data-parsley-validate data-parsley-trigger="change">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-7">

                    <input type="text" name="name" class="trackName" placeholder="Name">
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <h3>Question</h3>
                    <input type="text" name="question" placeholder="Ex: How happy are you? How many alcoholic drinks did you have?">
                </div>

                <div class="col-md-7">

                    <h3>Answers</h3>
                    The answers will be automatically displayed in the emails you will receive.  So, for example, if you
                    would like to track how much money you spend every day, you'd set the range from 0-10 and a multiplier
                    of x10 making the answers emailed to you every day between $0 and $100.  A custom value button is also
                    included in the email if you don't want to answer with any of your pre-set answers.
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start</label>
                            <select class="form-control" name="start" data-bind="value: start">
                                <option></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Finish</label>
                            <select class="form-control" name="finish" data-bind="value: finish, options: finishOptions">
                                <option></option>
                            </select>
                        </div>

                    </div>
                    <label>Multiplier</label>
                    <select class="form-control" name="interval" data-bind="value: interval">
                        <option value="1">x1</option>
                        <option value="10">x10</option>
                        <option value="100">x100</option>
                        <option value="1000">x1000</option>
                    </select>
                    <div class="preview" data-bind="visible: start() != undefined && start() != ''">
                        <label>Preview</label>
                        <div data-bind="foreach: intervalPreviewItems">
                            <a class="btn previewAnswer btn-success" data-bind="text: $data"></a>
                        </div>
                    </div>


                </div>

            </div>

            <div class="row questionsRow">
                <div class="col-md-7">
                    <h3>Scheduling</h3>
                    I would like to receive emails
                    <select class="form-control" name="schedule">
                        <option value="daily">Daily</option>
                        <option value="weekly">Weekly</option>
                        <option value="monthly">Monthly</option>
                    </select>
                    at
                    <select class="form-control" name="time">
                        <option value="12am">12am</option>
                        <option value="1am">1am</option>
                        <option value="2am">2am</option>
                        <option value="3am">3am</option>
                        <option value="4am">4am</option>
                        <option value="5am">5am</option>
                        <option value="6am">6am</option>
                        <option value="7am">7am</option>
                        <option value="8am">8am</option>
                        <option value="9am">9am</option>
                        <option value="10am">10am</option>
                        <option value="11am">11am</option>
                        <option value="12pm">12pm</option>
                        <option value="1pm">1pm</option>
                        <option value="2pm">2pm</option>
                        <option value="3pm">3pm</option>
                        <option value="4pm">4pm</option>
                        <option value="5pm">5pm</option>
                        <option value="6pm">6pm</option>
                        <option value="7pm">7pm</option>
                        <option value="8pm">8pm</option>
                        <option value="9pm">9pm</option>
                        <option value="10pm">10pm</option>
                        <option value="11pm">11pm</option>

                    </select>
                    <input type="hidden" data-bind="value: timezone" name="timezone">
                    <input type="hidden" data-bind="value: imageName" name="image">


                </div>
                <div class="col-md-7">
                    <h3>Customize <span class="optional">(optional)</span></h3>
                    <label>Units</label>
                    <input type="text" name="units" placeholder="Ex: hrs, calories, pages" style="margin-bottom: 20px;">

                    <label>Image</label>
                    <a href="#" data-bind="visible: imageName() == undefined" class="hvr-ripple-out selectImage"> <i class="fa fa-picture-o"></i> Select...</a>
                    <div data-bind="visible: imageName() != undefined" class="selectedImage">

                        <a class="btn btn-success change">Change</a>
                        <a class="btn btn-danger remove">Remove</a>
                    </div>
                </div>
            </div>
            <button class="createTrack btn btn-primary btn-lg">Create Track</button>
        </form>
    </div>


@stop

@section('footer')
    <script src="{{asset('bower/knockout/dist/knockout.js')}}"></script>
    <script src="{{asset('bower/jstz-detect/jstz.min.js')}}"></script>
    <script src="{{asset('js/dashboard/newTrack.js')}}"></script>
@stop