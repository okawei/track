@extends('layouts.loggedInNoSidebar')

@section('title')
    {{$track->name}}
@stop

@section('header')
    <link rel="stylesheet" href="{{asset('bower/sweetalert/dist/sweetalert.css')}}"/>
    <link rel="stylesheet" href="{{asset('/css/dashboard/viewTrack.css')}}">

    @if(count($points) <= 1)
        <style>
            #mainLineChart{
                opacity: 0.5 !important;
                filter: blur(20px);
            }
        </style>
    @endif
@stop


@section('content')
    <div class="header" style="background-image: url('{{asset('/images/tracks/'.$track->image)}}')">
        <a class="backToDash hvr-bounce-to-left" href="{{URL::to('/home')}}"><i class="fa fa-arrow-left"></i> Back to Dashboard</a>
        <div class="gradient"></div>
        <div  class="title">
            <h1>{{$track->name}}</h1>
            {{$track->question}}
        </div>
        <div class="controls">
            @if($track->disabled)
                <i class="fa fa-play-circle startTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Enable"></i>
            @else
                <i class="fa fa-minus-circle pauseTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Disable"></i>
            @endif
            <a href="{{URL::to('/track/edit/'.$track->id)}}"><i class="fa fa-pencil editTrack hvr-bounce-in" data-toggle="tooltip" data-placement="bottom" title="Edit"></i></a>
            <i class="fa fa-trash-o hvr-bounce-in deleteTrack" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>
        </div>
    </div>
    @if(count($points) <= 1)
        <div class="noData">
            <h1>Your Data Will Appear Here After 2 Days!</h1>
        </div>
    @endif
    <canvas id="mainLineChart"></canvas>
    <div class="container-fluid">

        <div class="row details1">
            <div class="col-md-6">

                <span class="records">
                    <div class="record">
                        <b>Record High: </b>
                        <span>{{count($track->getPointsArray($scale)) != 0 ? max($track->getPointsArray($scale)) : 0}} {{$track->units}}</span>
                    </div>

                    <div class="record">
                        <b>Record Low: </b>
                        <span>{{count($track->getPointsArray($scale)) != 0 ? min($track->getPointsArray($scale)) : 0}} {{$track->units}}</span>
                    </div>

                    <div class="record">
                        <b>Average: </b>
                        <span>{{ceil($average)}} {{$track->units}}</span>
                    </div>

                    <div class="record">
                        <b>Median: </b>
                        <span>{{$median}} {{$track->units}}</span>
                    </div>
                </span>
                <canvas id="barChart"></canvas>
            </div>
            <div class="col-md-6">

                <table class="table table-bordered">
                    <thead>
                        <th>Date</th>
                        <th>Value</th>
                    </thead>
                    <tbody>
                        @if(count($points) < 1)
                            <tr>
                                <td colspan="2" style="text-align: center;">No data has been added yet!</td>
                            </tr>
                        @endif
                        @foreach($points as $point)
                            <tr>
                                <td>
                                    {{date('m/d', strtotime($point->created_at))}}
                                </td>
                                <td>
                                    {{$point->points}}
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop


@section('footer')
    <script src="{{asset('bower/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('bower/Chart.js/Chart.min.js')}}"></script>
    <script>
        @if(count($points) > 1)
            var data = {{$track->getPointsArrayString($scale)}};
            var labels = {!! $track->getLabelsArrayString($scale) !!};
            var fillColor = "rgba(41, 128, 185,0.3)";
            var toolTipTemplate = "<%= value + ' "+units+"' %>";
            var strokeColor = "rgba(52, 152, 219,1.0)";
        @else
            var data = [50,50,70,80,100,70,40,40,50,10, 0];
            var labels = ["1/1", "1/2", "1/3", "1/4", "1/5", "1/6", "1/7", "1/8", "1/9", "1/10", "1/11"]
            var fillColor = "rgba(0,0,0,0.1)";
            var toolTipTemplate = "<%= value + ' "+units+"' %>";
            var strokeColor = "rgba(0,0,0,0.2)";
        @endif
        var trackId = {{$track->id}};
        var units = '{{$track->units}}';
        var trackName = '{{$track->name}}';
        var pauseURL = "{{URL::to('/track/pause/')}}";
        var startURL = "{{URL::to('/track/start/')}}";
        var editURL = "{{URL::to('/track/edit/')}}";
        var csrf_token = '{{csrf_token()}}';
    </script>
    <script src="{{asset('js/dashboard/viewTrack.js')}}"></script>


    @if(Session::has('success'))
        <script>
            swal({   title: "Success!",   text: "{{Session::get('success')}}",   type: "success",   confirmButtonText: "Sweet!" });
        </script>
    @endif
    @if(Session::has('error'))
        <script>
            swal({   title: "Error!",   text: "{{Session::get('error')}}",   type: "error",   confirmButtonText: "Well shoot..." });
        </script>
    @endif

    @if(count($points) <= 1)
        <script defer>

            document.getElementById("mainLineChart").style.webkitFilter = "blur(2px)";
        </script>
    @endif
@stop