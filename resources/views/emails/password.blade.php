<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>

    <link rel="stylesheet" href="ink.css"> <!-- For testing only -->

    <style type="text/css">

        *{
            font-family: Arial, helvetica, georgia, serif;
            color: #666;
            font-size: 16px;
        }

        img{
            width: 100%;
            max-width: 800px;
            border-radius: 5px 5px 0px 0px;
        }

        .center{
            padding: 20px;
            text-align: left;
        }

        #footer{
            width: 100%;
            max-width: 800px;
            background-color: #f2f2f2;
            color: #b2b2b2;
        }

        #footerText{
            padding: 20px;
            font-size: 12px;

            text-align: center;
        }

        #footerText > img{
            width: 80px;
            opacity: 0.8;
            height: auto;
        }
    </style>
</head>
<body>
<table class="body">
    <tr>
        <td class="center" align="center" valign="top" style="padding: 20px;">

            <img src="http://i.imgur.com/OaOAwLr.png">
            <div style="padding: 70px;">
                <h3>Dear User,</h3>

                Looks like you forgot your password, no worries, it happens to the best of us.  To reset your password go ahead
                and click the button below.  Thanks for using Track!
                <br><br>
                <div><!--[if mso]>
                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ url('password/reset/'.$token) }}" style="height:40px;v-text-anchor:middle;width:200px;" arcsize="10%" stroke="f" fillcolor="#2f79b9">
                        <w:anchorlock/>
                        <center>
                    <![endif]-->
                    <a href="{{ url('password/reset/'.$token) }}"
                       style="background-color:#2f79b9;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;">Reset My Password</a>
                    <!--[if mso]>
                    </center>
                    </v:roundrect>
             <![endif]--></div>
            </div>
            <div id="footer">
                <div id="footerText">
                    <img src="http://i.imgur.com/7FdKclg.png"><br>
                    Track is a website that allows you to track any metrics on your life through a daily email and generate beautiful reports on it.  Whether you're quitting smoking, losing weight or are just a data nerd track lets you gain metrics on your life.
                </div>
            </div>
        </td>
    </tr>
</table>
</body>
</html>