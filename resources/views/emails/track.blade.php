<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>

    <link rel="stylesheet" href="ink.css"> <!-- For testing only -->

    <style type="text/css">

        *{
            font-family: Arial, helvetica, georgia, serif;
            color: #666;
            font-size: 16px;
        }

        img{
            width: 100%;
            max-width: 800px;
            border-radius: 5px 5px 0px 0px;
        }

        .center{
            padding: 20px;
            text-align: left;
        }

        #footer{
            width: 100%;
            max-width: 800px;
            background-color: #f2f2f2;
            color: #b2b2b2;
        }

        #footerText{
            padding: 20px;
            font-size: 12px;

            text-align: center;
        }

        #footerText > img{
            width: 80px;
            opacity: 0.8;
            height: auto;
        }
    </style>
</head>
<body>
<table class="body">
    <tr>
        <td class="center" align="center" valign="top" style="padding: 20px;">
            <a href="{{URL::to('/track/unsubscribe/'.$track->secureId)}}" style="float: right; font-size: 12px; background-color: #e74c3c; color: #FFF; text-decoration: none; margin-right: 20px; padding: 5px; border-radius: 3px;">Stop Receiving This Email</a>
            <br><br>
            <div style="padding: 30px; margin: 20px; border: 1px solid #eeeeee; border-radius: 3px; background-image: url('http://trackemail.me/images/tracks/{{$track->image}}');">
                <h1 style="color: #FFF; background-color: rgba(0,0,0,0.1); padding: 10px; font-size: 30px; text-align: center; font-weight: 300;">{{$track->question}}</h1>


            </div>
            <a style="font-size: 22px; text-decoration: none; padding: 30px; margin: 20px; border: 1px solid #eeeeee; border-radius: 3px; text-align: center; display: block;" href="{{URL::to('/track/point/'.$track->secureId.'/custom')}}">Custom Value</a>

            @for($x = $track->start; $x <= $track->finish; $x++)
                <a style="font-size: 22px; text-decoration: none; padding: 30px; margin: 20px; border: 1px solid #eeeeee; border-radius: 3px; text-align: center; display: block;" href="{{URL::to('/track/point/'.$track->secureId.'/'.($x*$track->interval))}}">{{$x*$track->interval}}</a>

            @endfor


            <div id="footer">
                <div id="footerText">
                    <img src="http://i.imgur.com/7FdKclg.png" width="80" height="auto"><br>
                    Track is a website that allows you to track any metrics on your life through a daily email and generate beautiful reports on it.  Whether you're quitting smoking, losing weight or are just a data nerd track lets you gain metrics on your life.
                </div>
            </div>
        </td>
    </tr>
</table>
</body>
</html>