@extends('layouts.global')

@section('_title')

    @yield('title')

@stop

@section('_header')

    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    @yield('header')

@stop

@yield('beforeContent')


@section('_content')

        @yield('content')


@stop



@section('_footer')


    @yield('footer')

@stop