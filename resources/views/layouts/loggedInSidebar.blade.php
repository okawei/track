@extends('layouts.global')

@section('_title')

    @yield('title')

@stop

@section('_header')
    <link rel="stylesheet" href="{{asset('css/loggedIn.css')}}"/>
    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    @yield('header')

@stop

@yield('beforeContent')


@section('_content')
   <div class="sidebar">
        <div class="profilePic">
            @if(Auth::user()->profile != null)
                @if(Auth::user()->facebookId != 0)
                    <div class="pic" style="background-image: url('{{Auth::user()->profile}}');"></div>
                @else
                    <div class="pic" style="background-image: url('{{asset('images/dashboard/profilePlaceholder.png')}}');"></div>


                @endif
            @else
                <div class="pic" style="background-image: url('{{asset('images/dashboard/profilePlaceholder.png')}}');"></div>
            @endif
            <span class="name">
                {{Auth::user()->name}}
            </span>
        </div>


       <a class="sidebarItem hvr-sweep-to-right @if(Request::route()->getURI() == 'home') current @endif" href="{{URL::to('/home')}}">
           <i class="fa fa-bar-chart"></i>
           My Tracks
       </a>

       <a class="sidebarItem hvr-sweep-to-right @if(Request::route()->getURI() == 'track/new') current @endif" href="{{URL::to('/track/new')}}">
           <i class="fa fa-plus"></i>
           New Track
       </a>

       <a class="sidebarItem hvr-sweep-to-right @if(Request::route()->getURI() == 'account') current @endif" href="{{URL::to('/account')}}">
           <i class="fa fa-user"></i>
           My Account
       </a>

       <a class="sidebarItem hvr-sweep-to-right" href="{{URL::to('/logout')}}">
           <i class="fa fa-sign-out"></i>
           Log Out
       </a>

   </div>
   <div class="mobileControls">

       @if(Auth::user()->profile != null)
           @if(Auth::user()->facebookId != 0)
               <div class="profilePic" style="background-image: url('{{Auth::user()->profile}}');"></div>
           @else
               <div class="profilePic" style="background-image: url('{{asset('images/dashboard/profilePlaceholder.png')}}');"></div>


           @endif
       @else
           <div class="profilePic" style="background-image: url('{{asset('images/dashboard/profilePlaceholder.png')}}');"></div>
       @endif
       <span class="name">
                {{Auth::user()->name}}
            </span>

        <div class="hamburger">
            <i class="fa fa-bars showSidebar"></i>
            <i class="fa fa-times hideSidebar"></i>
        </div>
   </div>
   <div class="content">
       @yield('content')
   </div>

@stop



@section('_footer')
    <script src="{{asset('js/dashboard/loggedInSidebar.js')}}"></script>

    @yield('footer')

@stop