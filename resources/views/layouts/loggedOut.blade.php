@extends('layouts.global')

@section('_title')

    @yield('title')

@stop

@section('_header')
    <link rel="stylesheet" href="{{asset('css/loggedOut.css')}}"/>
    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    @yield('header')

@stop


@section('_content')

    <nav>
        <div class="logo">
            <a href="{{URL::to('/')}}">Track</a>
        </div>

        <div class="controls">
            <a class="hvr-grow" href="{{URL::to('/auth/register')}}">Sign Up</a>
            <a class="hvr-grow" href="{{URL::to('/auth/login')}}">Log In</a>
        </div>
    </nav>

    @yield('content')
@stop



@section('_footer')


    @yield('footer')

@stop