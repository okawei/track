@extends('layouts.global')

@section('_header')

    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="{{asset('/css/landing/message.css')}}">
@stop

@section('_content')
    <style>
        @if($message[0])

            body{
                background: url({{asset("images/landing/mountains2.jpg")}}) no-repeat 50% 50% fixed;
                background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -webkit-background-size: cover;
            }

        @else

            body{
                background: url({{asset("images/landing/mountains3.jpg")}}) no-repeat 50% 50% fixed;
                background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                -webkit-background-size: cover;
            }


        @endif
    </style>
    <div class="message">
        <a href="{{URL::to('/')}}"><img src="{{asset('images/logo-dark.png')}}"></a>
        <h2>{{$message[1]}}</h2>
        <h3>{!! $message[2] !!}</h3>

    </div>
@stop