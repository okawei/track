@extends('layouts.global')

@section('_header')

    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="{{asset('/css/points/custom.css')}}">
@stop

@section('_content')
    <div class="content">
        <header style="background-image: url('/images/tracks/{{$track->image}}');">
            <h2>
                {{$track->question}}
            </h2>
        </header>
        <div class="answer">
            @if(isset($point))
                <form action="/track/point/update/{{$point->id}}/custom" method="post">
            @else
                <form action="/track/point/{{$track->secureId}}/custom" method="post">
            @endif
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="number" class="form-control" name="value" placeholder="Enter a value" required>
                <br>
                <button class="btn-primary btn">Submit</button>
            </form>
        </div>

    </div>
@stop


@section('_footer')
    <script src="{{asset('/js/points/update.js')}}"></script>

@stop