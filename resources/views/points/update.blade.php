@extends('layouts.global')

@section('_header')

    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="{{asset('/css/points/update.css')}}">
    <style>
        body{
            background: url("{{asset('/images/landing/mountains2.jpg')}}");
            background-repeat:no-repeat;
            /* custom background-position */
            background-position:50% 50%;
            /* ie8- graceful degradation */
            background-position:50% 50% !important;
        }
    </style>
@stop

@section('_content')

    <header>
        <div class="questions">
            <h2>On <span class="date">{{date('m/d/Y', strtotime($point->updated_at))}}</span> you answered <span class="value">{{$point->points}}</span> to the question <span class="question">"{{$track->question}}"</span></h2>
            <h3>What would you like to change your answer to?</h3>
        </div>
    </header>

    <div class="answers">
        <div class="container">
            <a class="answer" href="{{URL::to('/track/point/update/'.$point->id.'/custom')}}">Custom Value</a>
            @for($x = $track->start; $x <= $track->finish; $x++)
                <a class="answer" href="{{URL::to('/track/point/update/'.$point->id.'/'.($x*$track->interval))}}">{{($x*$track->interval)}}</a>
            @endfor
        </div>
    </div>
@stop


@section('_footer')
    <script src="{{asset('/js/points/update.js')}}"></script>

@stop