<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Day Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('bower/bootstrap/dist/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('bower/Hover/css/hover.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/landing/landing.css')}}"/>

</head>
<body>

    <div class="logo">
        Track
    </div>

    <div class="controls">
        <a class="hvr-grow" href="{{URL::to('/auth/register')}}">Sign Up</a>
        <a class="hvr-grow" href="{{URL::to('/auth/login')}}">Log In</a>
    </div>

    <div class="signUp">
        <h1>Track Anything</h1>
        <h2>Get analytics on your life with beautiful reports using a simple email.</h2>
        <a class="signUpButton hvr-underline-from-center" href="{{URL::to('/auth/register')}}">Sign Up</a>
    </div>
    <div id="bg">
        <img src="{{asset('images/landing/banner.jpg')}}" alt="">
    </div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="bower/jquery/dist/jquery.js"><\/script>')</script>
<script src="{{asset('bower/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/landing.js')}}"></script>

<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

</body>
</html>